using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading.Tasks;


namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string startPath = @"E:\Ihsan\Visual Studio\Jarkom\Client\Client\bin\Debug";
                string zipPath = @"E:\Ihsan\Visual Studio\Jarkom\Client\Client\bin\Debug\jarkom compressed.zip";
                string encPath = @"E:\Ihsan\Visual Studio\Jarkom\Client\Client\bin\Debug\jarkom encrypted.zip";
                string sendFile = zipPath;

                Random rnd = new Random();
                int pass = rnd.Next();
                string seckey = pass.ToString();

                EncryptFile(startPath, encPath, seckey);
                Console.WriteLine("File Encrypted!");

                ZipFile.CreateFromDirectory(encPath, zipPath);
                Console.WriteLine("File Compressed!");

                TcpClient client = new TcpClient("127.0.0.1", 8080);
                Console.WriteLine("Connected. Sending File.");

                StreamWriter writer = new StreamWriter(client.GetStream());

                writer.WriteLine(seckey);
                writer.WriteLine("jarkom compressed.zip");

                byte[] bytes = File.ReadAllBytes(sendFile);

                writer.WriteLine(bytes.Length.ToString());
                writer.Flush();

                writer.WriteLine(sendFile);
                writer.Flush();

                Console.WriteLine("Sending File . . .");
                client.Client.SendFile(sendFile);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.Read();
        }

        public static byte[] Encrypt(byte[] byteToBeEncrypted,byte[] pass)
        {
            byte[] encrypted = null;
            byte[] salt = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using(MemoryStream ms=new MemoryStream())
            {
                using (RijndaelManaged rijn=new RijndaelManaged())
                {
                    rijn.KeySize = 256;
                    rijn.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(pass, salt, 1000);
                    rijn.Key = key.GetBytes(rijn.KeySize / 8);
                    rijn.IV = key.GetBytes(rijn.BlockSize / 8);
                    rijn.Mode = CipherMode.CBC;

                    using(var cs=new CryptoStream(ms,rijn.CreateEncryptor(),CryptoStreamMode.Write))
                    {
                        cs.Write(byteToBeEncrypted, 0, byteToBeEncrypted.Length);
                        cs.Close();
                    }
                    encrypted = ms.ToArray();
                }
            }
            return encrypted;
        }

        public static void EncryptFile(string file,string encryptedfile,string pass)
        {
            byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
            byte[] passBytes = Encoding.UTF8.GetBytes(pass);

            passBytes = SHA256.Create().ComputeHash(passBytes);

            byte[] bytesEncrypted = Encrypt(bytesToBeEncrypted, passBytes);

            File.WriteAllBytes(encryptedfile, bytesEncrypted);
        }
    }
}
